import bpy
from math import radians, degrees

def main():
    cube = bpy.data.objects['Cube.001']

    angle = cube['angle']
    cube.rotation_euler.y = radians(angle)


class CubeSpinOperator(bpy.types.Operator):
    """spins the cube!"""
    bl_idname = "object.cube_operator"
    bl_label = "Cube spin operator"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        main()
        return {'FINISHED'}

class CubeSpinPanel(bpy.types.Panel):
    """Spin panel"""
    bl_label="spin panel"
    bl_space_type='PROPERTIES'
    bl_region_type='WINDOW'
    bl_context='object'
    
    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        row.prop(bpy.data.objects['Cube.001'], '["angle"]', text = 'angle')
        
        row = layout.row()
        row.operator('object.cube_operator', text = 'Spin!')

def register():
    bpy.utils.register_class(CubeSpinPanel)
    bpy.utils.register_class(CubeSpinOperator)
    bpy.data.objects['Cube.001']['angle'] = 0


def unregister():
    bpy.utils.unregister_class(CubeSpinOperator)
    bpy.utils.unregister_class(CubeSpinPanel)


if __name__ == "__main__":
    register()